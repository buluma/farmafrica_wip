<?php
if (!defined('WEB_ROOT')) {
  exit;
}

// $sql = "SELECT o.f_id,f.farmer_id,o.farmer_id, o.owner_name,o.registration,o.owner_telephone,o.owner_gender,o.owner_telephone,o.finance_source,f.modified_by,o.date_modified,f.farm_county,f.farm_subcounty,f.farm_village,f.farm_name,r.id,r.type,u.id,u.name
//  from users u inner join farms f on u.id=f.modified_by inner join  farms_owners o on f.farmer_id=o.farmer_id  inner join farmer_reg_type r on o.registration=r.id
//  ORDER BY f_id";

 $sql = "SELECT * from harvest_information ORDER BY id";

 $result = dbQuery($sql);

// var_dump($result);

?> 

<div class="prepend-1 span-17">
<form action="processFarmerCosts.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="col-md-12">
<div class="table-responsive">
<table class="table table-striped table-bordered">
<thead>
  <tr>
   <td><b>ID</td>
   <td><b>Name of Farmer</td>
   <td><b>Name of Farm</td>
   <td><b>Pond No</td>
   <td><b>Harvest Since Last Visit</td>
   <td><b>Harvest Date</td>
   <td><b>Harvest Type</td>
   <td><b>Pieces Harvested</td>
   <td><b>Avg Weight Piece</td>
   <td><b>Total Weight KG</td>
   <td><b>Feed Type</td>
   <td><b>Production Cycle</td>
  <td><b>FCR</td>
  <td><b>Date Created</td>
  <td><b>Modified By</td>
  <td><b>modified_date</td>
    <td><b>Delete</td>
  </tr>
</thead>
<tbody>
<?php

while($row = dbFetchAssoc($result)) {
  extract($row);
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  // echo '<pre>';
  // var_dump($row);
  // echo '</pre>';
?>
  <tr class="<?php echo $class; ?>"> 
   <td><a href="javascript:editfarmsampling(<?php echo $id; ?>);"><?php echo $id; ?></a></td>
   <td><?php echo $owner_name; ?></td>
   <td><?php echo $farm_id; ?></td>
   <td><?php echo $pond_number; ?></td>
   <td><?php echo $havest_since_last_visit; ?></td>
   <td><?php echo $harvest_date; ?></td>
   <td><?php echo $harvest_type; ?></td>
   <td><?php echo $pieces_harvested; ?></td>
   <td><?php echo $avg_weight_piece; ?></td>
   <td><?php echo $total_weight_kg; ?></td>
   <td><?php echo $feed_type; ?></td>
   <td><?php echo $production_cycle; ?></td>
   <td><?php echo $fcr; ?></td>
   <td><?php echo $date_created; ?></td>
   <td><?php echo $modified_by; ?></td>
   <td><?php echo $modified_date; ?></td>
   <td align="center"><a href="javascript:delete(<?php echo $id; ?>);">Delete</a></td>
  </tr>
<?php

} // end while

?>
  <tr> 
   <td colspan="17">&nbsp;#</td>
  </tr>
  <tr> 
   <td colspan="17" align="right"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Farm Harvest (+)" class="button" onClick="addcosts()"></td>
  </tr>
 </tbody>
</table>
</div>
</div>
</form>
</div>