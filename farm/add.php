<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$sql = "SELECT farmer_id, owner_name FROM farms_owners order by f_id desc
limit 1";
$result = dbQuery($sql);
while($row = dbFetchAssoc($result)) {
    extract($row);
   }


?> 

<div class="prepend-1 span-12">
<h4>Add farm information for:<?php echo $owner_name; ?> ID number <?php echo $farmer_id; ?></h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-sm-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=add" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
 
   <div class="form-group row">
   <input class="form-control" name="farmer" type="hidden" id="farmer" value="<?php echo $owner_name; ?>" disabled></label>
   <input class="form-control" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" readonly></label>
   </div>
    <!-- <div class="form-group row">
        <p onclick="geoFindMe()" type="button" class="btn btn-default btn-lg">
      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></p>
    <div id="out"></div>
  </div> -->
   <div class="form-group row">
   <label for="farm_name" class="col-sm-3 col-form-label">Name of Farm:<input class="form-control" name="farm_name" type="text" id="farm_name" value="" required ></label>
   </div>
   <div class="form-group row" >
    <label for="farm_county" class="col-sm-3 col-form-label">County :<select class="form-control" name="farm_county" type="text" id="farm_county"  required="" ><?php getCounties();?></select></label></label>
   <label for="farm_subcounty" class="col-sm-3 col-form-label">SubCounty:<select class="form-control" name="farm_subcounty" type="text" id="farm_subcounty"  required="" ><?php getSubcounties();?></select></label></label>
    <input class="form-control" name="farm_longitude" type="hidden" id="out" value="" required="" >
   <input class="form-control" name="farm_latitude" type="hidden" id="farm_latitude" value="" required="" >
   <label for="farm_village" class="col-sm-3 col-form-label">Village:<input class="form-control" name="farm_village" type="text" id="farm_village" value="" required="" ></label>
   
  </div>

   <div class="form-group row" >
   <!-- <label for="pond_sample_temperature" class="col-sm-3 col-form-label">Temperature of sampled pond:<input class="form-control" name="pond_sample_temperature" type="number" id="pond_sample_temperature" value="<?php echo $pond_sample_temperature; ?>" required="" ></label>
   <label for="pond_sample_time" class="col-sm-3 col-form-label">Time of day:<select class="form-control" name="pond_sample_time" type="text" id="pond_sample_time"  required="" >
    <option value="" >--select--</option>
    <option value="Morning">Morning</option>
    <option value="Noon">Noon</option>
    <option value="Evening">Evening</option>
    </select></label> -->
    <label for="farm_landmark" class="col-sm-3 col-form-label">Landmark:<input class="form-control" name="farm_landmark" type="text" id="farm_landmark" value="" required="" ></label>
    <label for="ponds_number" class="col-sm-3 col-form-label">Total Number of Ponds:<input class="form-control" name="ponds_number" type="number" id="ponds_number" value="0" required="" ></label>
   <label for="ponds_stocked" class="col-sm-3 col-form-label">Ponds Stocked:<input class="form-control" name="ponds_stocked" type="number" id="ponds_stocked" value="0"  required="" ></label>
   </div>
  <div class="form-group row" >
  <label for="keeps_records" class="col-sm-3 col-form-label">Do they keep records?</label>
    Yes <input type="radio" onclick="javascript:yesnoCheck();" name="keeps_records" value="Yes" id="yesCheck"> No <input type="radio" onclick="javascript:yesnoCheck();" name="keeps_records" value="No" id="noCheck">
    <div id="ifYes" style="visibility:hidden">
    <label for="record_keeping" class="col-sm-3 col-form-label">How are records kept:<select class="form-control" id ="record_keeping" name="record_keeping">
    <option></option>
    <option value="Manually">Manually</option>
    <option value="Computer">Computer</option>
    </select></label>
        
     <label for="records_kept" class="col-sm-3 col-form-label">What types of records are kept:<select class="form-control" name="records_kept">
    <option ></option>
    <option value="Bank">Bank</option>
    <option value="Book">Book</option>
    <option value="Book and File">Book and File</option>
    <option value="Book/Computer">Book/Computer</option>
    </select></label>
  </div>

  </div>
  <div class="form-group row" >
  <label for="has_business_plan" class="col-sm-3 col-form-label">Does he/she have a business plan?</label>
    Yes <input type="radio" onclick="javascript:businessplan();" name="has_business_plan" value="Yes" id="yes"> No <input type="radio" onclick="javascript:businessplan();" name="has_business_plan" value="No" id="no">
    <div id="iftrue" style="visibility:hidden">
    <label for="business_plan_last_update" class="col-sm-3 col-form-label">When was the Last time of business plan update:<input class="form-control" name="business_plan_last_update" type="date" id="business_plan_last_update" value="" required="" ></label>
        
  </div>
  
  </div>
  

 <p align="center"> 
  <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="checkAddFarmForm();" class="box"> 
 </p>
</form>
 </tbody>
</table>
</div>


</div>