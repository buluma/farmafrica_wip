<?php
if (!defined('WEB_ROOT')) {
  exit;
}

// $sql = "SELECT o.f_id,f.farmer_id,o.farmer_id, o.owner_name,o.registration,o.owner_telephone,o.owner_gender,o.owner_telephone,o.finance_source,f.modified_by,o.date_modified,f.farm_county,f.farm_subcounty,f.farm_village,f.farm_name,r.id,r.type,u.id,u.name
//  from users u inner join farms f on u.id=f.modified_by inner join  farms_owners o on f.farmer_id=o.farmer_id  inner join farmer_reg_type r on o.registration=r.id
//  ORDER BY f_id";

 $sql = "SELECT * from farm_costs ORDER BY id";

 $result = dbQuery($sql);

// var_dump($result);

?> 

<div class="prepend-1 span-17">
<form action="processFarmerCosts.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="col-md-12">
<div class="table-responsive">
<table class="table table-striped table-bordered">
<thead>
  <tr>
   <td><b>ID</td>
   <td><b>Name of Farmer</td>
   <td><b>Name of Farm</td>
   <td><b>Cost Type</td>
   <td><b>Year</td>
   <td><b>Month</td>
   <td><b>Amount</td>
   <td><b>Date Created</td>
   <td><b>Modified By</td>
   <td><b>Date modified</td>
   <td><b>Delete</td>
  </tr>
</thead>
<tbody>
<?php

while($row = dbFetchAssoc($result)) {
  extract($row);
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  // echo '<pre>';
  // var_dump($row);
  // echo '</pre>';
?>
  <tr class="<?php echo $class; ?>"> 
   <td><a href="javascript:editfarmcosts(<?php echo $id; ?>);"><?php echo $id; ?></a></td>
   <td><?php echo $owner_name; ?></td>
   <td><?php echo $farm_id; ?></td>
   <td><?php echo $cost_type; ?></td>
   <td><?php echo $year; ?></td>
   <td><?php echo $month; ?></td>
   <td><?php echo $amount; ?></td>
   <td><?php echo $date_created; ?></td>
   <td><?php echo $modified_by; ?></td>
   <td><?php echo $modified_date; ?></td>
   <td align="center"><a href="javascript:delete(<?php echo $id; ?>);">Delete</a></td>
  </tr>
<?php

} // end while

?>
  <tr> 
   <td colspan="14">&nbsp;#</td>
  </tr>
  <tr> 
   <td colspan="14" align="right"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Farm Cost (+)" class="button" onClick="addcosts()"></td>
  </tr>
 </tbody>
</table>
</div>
</div>
</form>
</div>