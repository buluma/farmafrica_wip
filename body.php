<div id="wrapper">
<nav class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
          <li class="nav-header">
            <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                             </span>
              <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Stephen Muganda</strong>
                </span> <span class="text-muted text-xs block">Super User <b class="caret"></b></span> </span>
              </a>
              <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a href="profile.html">Profile</a></li>
                <li><a href="contacts.html">Contacts</a></li>
                <li><a href="mailbox.html">Mailbox</a></li>
                <li class="divider"></li>
                <li><a href="../../login.html">Logout</a></li>
              </ul>
            </div>
            <div class="logo-element">
              IN+
            </div>
          </li>
          <li class="active">
            <a href="index.php"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span> <span class="fa arrow"></span></a>
            <!-- <ul class="nav nav-second-level">
              <li class="active"><a href="#">Dashboard</a></li>
            </ul> -->
          </li>
          <li>
            <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Farm Profiles</span>  <span class="pull-right label label-primary">SPECIAL</span></a>
            <ul class="nav nav-second-level">
              <li><a href="index.php?view=farmers">Farm Profiles</a></li>
              <li><a href="#">Farm Costs</a></li>
              <li><a href="#">Farm Sales</a></li>
              <li><a href="#">Sampling Feed</a></li>
              <li><a href="#">Harvest Information</a></li>
              <li><a href="#">Fish Market</a></li>
              <li><a href="#">Calendar of Visits</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">SMS Messages </span><span class="label label-warning pull-right">16/24</span></a>
            <ul class="nav nav-second-level">
              <li><a href="messages.html">Inbox</a></li>
              <li><a href="sms_detail.html">Message view</a></li>
              <li><a href="sms_compose.html">Compose Message</a></li>
              <li><a href="#">Surveys & Polls</a></li>
              <li><a href="#">Groups Management</a></li>
              <li><a href="#">Contact Management</a></li>
              <li><a href="#">SMS templates</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-calendar"></i> <span class="nav-label">Training</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="#">Training Modules</a></li>
              <li><a href="#">Training Sessions</a></li>
              <li><a href="#">Training Calendar</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Reports</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="#">Pond Information</a></li>
              <li><a href="#">Farm Harvests</a></li>
              <li><a href="#">Farm Feeds</a></li>
              <li><a href="#">Farmer Training</a></li>
              <li><a href="#">Product Cycle Report</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="#">User Levels</a></li>
              <li><a href="#">User Permissions</a></li>
              <li><a href="<?php echo WEB_ROOT; ?>menu.php?v=USER" title="Manage Users">Manage Users</a>">Staff Members</a></li>
              <li><a href="#">Field agents</a></li>
            </ul>
          </li>
        </ul>

      </div>
    </nav>
    </div>