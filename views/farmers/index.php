<h1>Farmers List</h1>
<hr>
<?php

//echo $data['filters'];
foreach ($data['html'] as $key => $html) {
	echo $html;
}

// echo '<pre>';
// print_r($data);
// echo '</pre>';

?>

<table class="table table-striped table-bordered">
<thead>
	<tr>
		<?php 
			echo '<th></th>';
			foreach ($data['labels'] as $key => $label) {
				echo '<th>'.$label.'</th>';
			}
		?>
	</tr>
</thead>
<tbody>
	<?php
		foreach ($data['items'] as $key => $item) {
			echo '<tr>';
			// actions column
			echo '<td>';
			echo '<a class="btn btn-primary btn-sm" href="index.php?view=farmers&action=view&id='.$item->id.'"> View </a>';
			echo '<a class="btn btn-danger btn-sm" href="index.php?view=farmers&action=edit&id='.$item->id.'"> Edit </a>';
			echo '</td>';
			foreach ($item as $field => $value) {
				echo '<td>'.$value.'</td>';
			}
			echo '</tr>';
		}
	?>
</tbody>
</table>