<h1>New Farmer</h1>

<hr>
<?php

$item = $data['items'][0];
$item = (array)$item;
$labels = $data['labels'];
$item_fields = array_keys($item);
$item_values = array_values($item);
// echo '<pre>';
// print_r($item);
// print_r($item_fields);
// print_r($item_values);
// echo '</pre>';

?>
<div class="col-md-12">
	<a class="btn button btn-small btn-default" href="index.php?view=farmers&action=edit&id=<?php echo $item['id']; ?>"> Edit </a>
</div>

<div class="col-md-5">
<table class="table table-striped table-bordered">
<thead>
	<tr>
		<th></th>
		<th></th>
	</tr>
</thead>
<tbody>
	<?php 
		foreach ($labels as $key => $label) {
			echo '<tr>';
			echo '<th>'.$label.'</th>';
			echo '<td>'.$item_values[$key].'</td>';
			echo '</tr>';
		}
	?>
</tbody>
</table>
</div>